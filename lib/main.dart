import 'package:flutter/material.dart';
import 'play.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: Center(
            child: FractionallySizedBox(
          widthFactor: 1,
          heightFactor: 0.8,
          child: VideoPlayerScreen(),
        )),
      ),
    );
  }
}
